const { quat, mat4, vec3 } = glMatrix

class TinyModel {
    constructor(renderer) {
        this.renderer = renderer
        this.scale = vec3.fromValues(1.0, 1.0, 1.0)
        this.position = vec3.fromValues(0.0, 0.0, 0.0)
        this.rotation = quat.fromValues(0.0, 0.0, 0.0, 1.0)
        this.modelName = ''
        this.materials = []
        this.meshes = []
    }

    getWorld() {
        //const translation = mat4.translate(mat4.create(), mat4.create(), this.position)
        //let rotation = mat4.create()
        //const scaleRot = m4.multiply(m4.scale(m4.identity(), this.scale), this.rotation.toRotationMatrix())
        //return m4.multiply(scaleRot, translation)
        return mat4.create()
    }

    meshFactory(meshType, model) {
        const { renderer } = this
        switch (meshType) {
            case MESHTYPE_STANDARD:
                return new RenderableSingleMesh(renderer)

            case MESHTYPE_SECTOR:
            case MESHTYPE_DUMMY:
                return new GeneralMesh(renderer)

            default:
                console.log(`[*] unsuported mesh type: ${meshType}`)
                return null
        }
    }

    initFromMafia(model) {
        const { renderer } = this

        //NOTE: load materials ( texture 4now ) for model 
        if (model.materials.length > 0) {
            for (const material of model.materials) {
                this.materials.push(twgl.createTexture(renderer.gl, {
                    src: encodeURI('maps/' + material.difuseMapName),
                    mag: renderer.gl.LINEAR
                }))
            }
        }

        //NOTE: create instances of meshes by mesh type
        for (const mesh of model.meshes) {
            const newRenderable = this.meshFactory(mesh.meshType)
            if (newRenderable === null)
                continue;

            newRenderable.initFromMafia(mesh, this.materials, this)
            this.meshes.push(newRenderable)
        }

        //NOTE: fix parrenting here ?
        renderer.objects.push(this)
    }

    //NOTE: render all submeshes of model
    render(time) {
        //this.position[0] += 0.4 * time
        const modelWorld = this.getWorld()
        for (let mesh of this.meshes) {
            if (mesh.render !== undefined) {
                mesh.render(time, modelWorld)
            }
        }
    }
}

function perspectiveLH(out, fovy, aspect, near, far) {
    let f = 1.0 / Math.tan(fovy / 2),
        nf;
    out[0] = -f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[15] = 0;
    if (far != null && far !== Infinity) {
        nf = 1 / (near - far);
        out[10] = (far + near) * nf;
        out[14] = 2 * far * near * nf;
    } else {
        out[10] = -1;
        out[14] = -2 * near;
    }
    return out;
}

class GeneralMesh {
    constructor(renderer) {
        this.renderer = renderer
        this.scale = vec3.fromValues(1.0, 1.0, 1.0)
        this.position = vec3.fromValues(0.0, 0.0, 0.0)
        this.rotation = quat.fromValues(0.0, 0.0, 0.0, 1.0)
        this.parentId = 0 // NOTE 0 - not connected
        this.meshName = 'Unknown'
        this.meshParams = ''
        this.model = null

        this.cachedWorld = null
    }

    getParrent() {
        return this.model.meshes[this.parentId - 1]
    }

    // pos * rot * scale 
    getWorld() {
        if(this.cachedWorld === null) {
            const translation = mat4.translate(mat4.create(), mat4.create(), this.position)
            const scale = mat4.scale(mat4.create(), mat4.create(), this.scale)
            const rotation = mat4.fromQuat(mat4.create(), this.rotation)
    
            let world = mat4.multiply(mat4.create(), mat4.multiply(mat4.create(), translation, rotation), scale)
            this.cachedWorld = mat4.multiply(mat4.create(), this.parentId !== 0 ? this.getParrent().getWorld() : mat4.create(), world)
        }
       
        return this.cachedWorld
    }

    initFromMafia(mesh, materials, model) {
        this.model = model
        this.parentId = mesh.parentID
        this.meshName = mesh.meshName
        this.meshParams = mesh.meshParams

        this.position = vec3.fromValues(mesh.pos.x, mesh.pos.y, mesh.pos.z)
        this.scale = vec3.fromValues(mesh.scale.x, mesh.scale.y, mesh.scale.z)
        this.rotation = quat.fromValues(mesh.rot.x, mesh.rot.y, mesh.rot.z, mesh.rot.w * -1)
    }

    isVisible() {
        return !(this.meshName.indexOf('wcol') > -1 ||
            this.meshParams.indexOf('OFF_') > -1 ||
            this.meshParams.indexOf('off_') > -1)
    }
}

const _d = (id, value, callback = null) => {
    const elm = document.getElementById(id)

    if (document.activeElement != elm)
        elm.value = value

    if (callback !== null && elm.onchange === null) {
        elm.onchange = (e) => callback(e.target.value)
    }
}

class RenderableSingleMesh extends GeneralMesh {
    constructor(renderer) {
        super(renderer)
        const { gl } = renderer
        this.gl = gl
        this.renderer = renderer
        this.faceGroups = []
        this.materials = []
        this.vertexBuffer = gl.createBuffer()
    }

    initFromMafia = (mafiaMesh, materials, model) => {
        super.initFromMafia(mafiaMesh, materials, model)

        const { gl } = this
        const firstLOD = mafiaMesh.standard.lods[0]

        //NOTE: skip wcol meshes ( collision ) or singlemeshes without lods
        if (firstLOD === undefined || !super.isVisible())
            return

        for (let faceGroup of firstLOD.faceGroups) {
            //NOTE: create index buffer per face
            const indices = []
            const indexBuffer = gl.createBuffer()
            for (let face of faceGroup.faces) {
                indices.push(face.A)
                indices.push(face.B)
                indices.push(face.C)
            }

            //NOTE: now send the element array to GL
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer)
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW)

            const newFaceGroup = {
                material: materials[faceGroup.materialID - 1],
                indexBuffer,
                elementCount: faceGroup.faces.length
            }

            this.faceGroups.push(newFaceGroup)
        }

        const vertices = []
        for (let vertex of firstLOD.vertices) {
            vertices.push(vertex.pos.x)
            vertices.push(vertex.pos.y)
            vertices.push(vertex.pos.z)

            vertices.push(vertex.normal.x)
            vertices.push(vertex.normal.y)
            vertices.push(vertex.normal.z)

            vertices.push(vertex.uv.x)
            vertices.push(vertex.uv.y)
        }

        // now pass the list of positions into WebGL to build the
        // shape. We do this by creating a Float32Array from the
        // JavaScript array, then use it to fill the current buffer.
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    }

    render(time, modelWorld) {

        const { gl, renderer, bufferInfo } = this
        const { programInfo } = renderer
        const { viewProjection } = renderer.camera

        const world = this.getWorld() //mat4.multiply(mat4.create(), modelWorld, this.getWorld())   = 
        const worldViewProj = mat4.multiply(mat4.create(), viewProjection, world)
        const worldInverseTranspose = mat4.transpose(mat4.create(), mat4.invert(mat4.create(), world))

        gl.useProgram(programInfo.program)
        renderer.setTransformUnifroms(worldViewProj, worldInverseTranspose)
     
        //NOTE: bind vertices
        const vertexPosition = renderer.attribs.position
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        gl.vertexAttribPointer(vertexPosition, 3, gl.FLOAT, false, 32, 0);
        gl.enableVertexAttribArray(vertexPosition);

        //NOTE: bind normals
        const normalPosition = renderer.attribs.normal
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        gl.vertexAttribPointer(normalPosition, 3, gl.FLOAT, true, 32, 12);
        gl.enableVertexAttribArray(normalPosition);

        //NOTE: bind tex coords
        const texPosition = renderer.attribs.texcoord
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        gl.vertexAttribPointer(texPosition, 2, gl.FLOAT, false, 32, 24);
        gl.enableVertexAttribArray(texPosition);

        //NOTE: bind index buffer per face and update uniforms
        for (let faceGroup of this.faceGroups) {
            
            gl.activeTexture(gl.TEXTURE0)
            gl.bindTexture(gl.TEXTURE_2D, faceGroup.material)
            renderer.setTextureUniform(gl.TEXTURE0)

            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, faceGroup.indexBuffer)
            gl.drawElements(gl.TRIANGLES, faceGroup.elementCount * 3, gl.UNSIGNED_SHORT, 0)
        }
    }
}

class Camera {
    constructor(renderer) {
        const { gl } = renderer
        this.gl = gl
        this.renderer = renderer
        this.speed = 30.0
        this.camera = mat4.create()
        this.view = mat4.create()
        this.projection = mat4.create()
        this.viewProjection = mat4.create()
        this.position = vec3.fromValues(-1.0, -1.0, -15.5)
        this.forward = vec3.fromValues(0.0, 0.0, 0.0)
        this.right = vec3.fromValues(0.0, 0.0, 0.0)
        this.angles = { x: 0.0, y: 0.0 }

        const fov = 62 * Math.PI / 180
        const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight
        const zNear = 1
        const zFar = 8000
        this.projection = perspectiveLH(mat4.create(), fov, aspect, zNear, zFar)
    }

    render(time) {
        this.processControlls(time)
        this.view = mat4.invert(mat4.create(), this.camera)
        this.viewProjection = mat4.multiply(mat4.create(), this.projection, this.view)
    }

    processControlls(time) {
        const { keys, lastMouseDelta } = this.renderer
        const scalarSpeed = this.speed * time

        if (keys['KeyW']) {
            const speed = vec3.fromValues(scalarSpeed, scalarSpeed, scalarSpeed)
            this.position = vec3.add(vec3.create(), this.position, vec3.multiply(vec3.create(), this.forward, speed))
        }

        if (keys['KeyA']) {
            const speed = vec3.fromValues(scalarSpeed, scalarSpeed, scalarSpeed)
            this.position = vec3.add(vec3.create(), this.position, vec3.multiply(vec3.create(), this.right, speed))
        }

        if (keys['KeyS']) {
            const speed = vec3.fromValues(-scalarSpeed, -scalarSpeed, -scalarSpeed)
            this.position = vec3.add(vec3.create(), this.position, vec3.multiply(vec3.create(), this.forward, speed))
        }

        if (keys['KeyD']) {
            const speed = vec3.fromValues(-scalarSpeed, -scalarSpeed, -scalarSpeed)
            this.position = vec3.add(vec3.create(), this.position, vec3.multiply(vec3.create(), this.right, speed))
        }

        _d("xPos", this.position[0], (newval) => this.position[0] = Number(newval))
        _d("yPos", this.position[1], (newval) => this.position[1] = Number(newval))
        _d("zPos", this.position[2], (newval) => this.position[2] = Number(newval))
        _d("camSpeed", this.speed, (newval) => this.speed = Number(newval))

        const rotY = mat4.fromYRotation(mat4.create(), this.angles.x)
        const rotX = mat4.fromXRotation(mat4.create(), -this.angles.y)
        const translation = mat4.translate(mat4.create(), mat4.create(), vec3.multiply(vec3.create(), this.position, vec3.fromValues(-1.0, -1.0, -1.0)))

        this.camera = mat4.multiply(mat4.create(), translation, mat4.multiply(mat4.create(), rotY, rotX))
        this.forward = vec3.fromValues(this.camera[8], this.camera[9], this.camera[10])
        this.right = vec3.cross(vec3.create(), this.forward, vec3.fromValues(0.0, 1.0, 0.0))
    }

    mouseMove(deltaX, deltaY) {
        this.angles.x += deltaX * 0.15 * this.renderer.deltaTime
        this.angles.y += deltaY * 0.15 * this.renderer.deltaTime
    }
}

class Renderer {
    constructor(canvasId) {
        this.canvas = document.querySelector(canvasId)
        this.gl = this.canvas.getContext('webgl')
        this.programInfo = null
        this.camera = null
        this.objects = []
        this.keys = {}
        this.uniforms = {}
        this.attribs = {}
        this.deltaTime = (16 / 1000.0)
        this.init()
    }

    init() {
        this.initPointerLock()
        this.bindInput()

        const { gl } = this
        this.programInfo = twgl.createProgramInfo(gl, ["vs", "fs"])
        this.camera = new Camera(this)

        //NOTE: setup stats panel
        this.stats = new Stats()
        this.stats.showPanel(0)
        document.body.appendChild(this.stats.dom)

        //NOTE: setup webgl stuff
        twgl.resizeCanvasToDisplaySize(gl.canvas)
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)
        gl.enable(gl.DEPTH_TEST)
        gl.enable(gl.CULL_FACE)
        gl.enable(gl.BLEND)
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
        gl.cullFace(gl.FRONT)

        const { program } = this.programInfo

        //NOTE: get pointer attribs
        this.attribs.position = gl.getAttribLocation(program, 'position')
        this.attribs.normal = gl.getAttribLocation(program, 'normal')
        this.attribs.texcoord = gl.getAttribLocation(program, 'texcoord')

        //NOTE: get uniforms 
        this.uniforms.worldViewProjection = gl.getUniformLocation(program, "u_worldViewProjection")
        this.uniforms.worldInverseTranspose = gl.getUniformLocation(program, "u_worldInverseTranspose")
        this.uniforms.diffuse = gl.getUniformLocation(program, "u_diffuse")
    }

    setTransformUnifroms(mvpVal, invertWorldTransposeVal) {
        const { gl } = this
        const { worldViewProjection, worldInverseTranspose } = this.uniforms
        gl.uniformMatrix4fv(worldViewProjection, false, mvpVal)
        gl.uniformMatrix4fv(worldInverseTranspose, false, invertWorldTransposeVal)
    }

    setColorKeyUniform(tex) {
        const { gl } = this
        
    }

    setTextureUniform(tex) {
        const { gl } = this
        const { diffuse } = this.uniforms
        gl.uniform1i(diffuse, tex)
    }

    bindInput() {
        const onKeyPress = (e, isDown) => {
            this.keys[e.code] = isDown
        }

        document.onkeydown = (e) => onKeyPress(e, true)
        document.onkeyup = (e)   => onKeyPress(e, false)
    }

    initPointerLock() {
        this.canvas.requestPointerLock = this.canvas.requestPointerLock || this.canvas.mozRequestPointerLock || this.canvas.webkitRequestPointerLock
        this.canvas.onclick = () => {
            this.canvas.requestPointerLock()
            //this.canvas.requestFullscreen()
        }

        this.canvas.requestPointerLock()

        document.onmousemove = (e) => {
            if (document.pointerLockElement === this.canvas ||
                document.mozPointerLockElement === this.canvas ||
                document.webkitPointerLockElement === this.canvas) {
                this.camera.mouseMove(e.movementX, e.movementY)
            }
        }
    }

    render(time) {
        this.stats.begin()

        const { gl } = this
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
        gl.clearColor(0.07, 0.07, 0.27, 255)

        this.camera.render(time)

        let meshesToDraw = 0
        for (const object of this.objects) {
            object.render(time)
            meshesToDraw += object.meshes.length
        }

        _d("meshCount", meshesToDraw)
        this.stats.end()
    }
}

// NOTE: main engine loop
let tinyRenderer = new Renderer('#mainCanvas')
let lastTime = new Date()
const render = () => {
    const currentTime = new Date()
    tinyRenderer.render((currentTime - lastTime) / 1000.0)
    lastTime = currentTime
    requestAnimationFrame(render)
}

// NOTE: used for loading models into scene 
let fileInput = document.querySelector('#filePicker');
let fileReader = new FileReader();
fileReader.onload = (e) => {
    let newModel = new Model();
    if (newModel.load(e.target.result)) {

        //NOTE: init our Model from mafia model 
        const newTinyModel = new TinyModel(tinyRenderer)
        newTinyModel.initFromMafia(newModel)
    }
}

fileInput.onchange = (e) => {
    let file = fileInput.files[0];
    fileReader.readAsArrayBuffer(file);
}

render()