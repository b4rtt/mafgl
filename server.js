const express   = require('express')
const app       = express()
const port      = 8080
const fs        = require('fs');

/*app.get('/maps/:file', (req, res)=> {

    // NOTE: our hack for encoding __ instead of % 
    // 
    if(req.params.file.indexOf('__') > -1) {
        req.params.file = req.params.file.replace('__', '%')   
    }

    fs.readFile('./maps/' + req.params.file, (err, data) => {
        if(err) {
            res.send("Oops! Couldn't find that file.");
            console.log('Oops! Couldnt find that file.', './maps' + req.params.file)
        } else {
            res.contentType(req.params.file);
            res.send(data);
        }   
        res.end();
    }); 
});*/

app.use(express.static('.'))
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))