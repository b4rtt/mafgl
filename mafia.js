class BinaryReader {
    constructor(buffer) {
        this.dataView = new DataView(buffer);
        this.offset = 0;
    }

    readFloat = ()=> {
        let returnVal = this.dataView.getFloat32(this.offset, true);
        this.offset += 4;
        return returnVal;
    }

    readVec = ()=> {
        return {x: this.readFloat(), y: this.readFloat(), z: this.readFloat()};
    }

    readQuat = ()=> {
        return {w: this.readFloat(), x: this.readFloat(), y: this.readFloat(), z: this.readFloat()};
    }

    readU8 = ()=> {
        let returnVal = this.dataView.getUint8(this.offset);
        this.offset++;
        return returnVal;
    }

    readU16 = ()=> {
        let returnVal = Number(this.dataView.getUint16(this.offset, true));
        this.offset += 2;
        return returnVal;
    }

    readU32 = ()=> {
        let returnVal = this.dataView.getUint32(this.offset, true);
        this.offset += 4;
        return returnVal;
    }

    readU64 = ()=> {
        let a = this.readU32();
        let b = this.readU32();

        return (a << 32 | b);
    }

    readChar = ()=> {
        return String.fromCharCode(this.readU8());
    }

    readString = (len) => {
        let currentString = '';
        for(let i = 0; i < len; i++) {
            currentString += this.readChar();
        }

        return currentString;
    } 
}

const MATERIALFLAG_TEXTUREDIFFUSE = 0x00040000;         // whether diffuse texture is present
const MATERIALFLAG_COLORED = 0x08000000;                 // whether to use diffuse color (only applies with diffuse texture)
const MATERIALFLAG_MIPMAPPING = 0x00800000;
const MATERIALFLAG_ANIMATEDTEXTUREDIFFUSE = 0x04000000;
const MATERIALFLAG_ANIMATEXTEXTUREALPHA = 0x02000000;
const MATERIALFLAG_DOUBLESIDEDMATERIAL = 0x10000000;     // whether backface culling should be off
const MATERIALFLAG_ENVIRONMENTMAP = 0x00080000;          // simulates glossy material with environment texture
const MATERIALFLAG_NORMALTEXTUREBLEND = 0x00000100;      // blend between diffuse and environment texture normally
const MATERIALFLAG_MULTIPLYTEXTUREBLEND = 0x00000200;    // blend between diffuse and environment texture by multiplying
const MATERIALFLAG_ADDITIVETEXTUREBLEND = 0x00000400;    // blend between diffuse and environment texture by addition
const MATERIALFLAG_CALCREFLECTTEXTUREY = 0x00001000;
const MATERIALFLAG_PROJECTREFLECTTEXTUREY = 0x00002000;
const MATERIALFLAG_PROJECTREFLECTTEXTUREZ = 0x00004000;
const MATERIALFLAG_ADDITIONALEFFECT = 0x00008000;       // should be ALPHATEXTURE | COLORKEY | ADDITIVEMIXING
const MATERIALFLAG_ALPHATEXTURE = 0x40000000;
const MATERIALFLAG_COLORKEY = 0x20000000;
const MATERIALFLAG_ADDITIVEMIXING = 0x80000000;           // the object is blended against the world by adding RGB (see street lamps etc.)


const MESHTYPE_STANDARD = 0x01,           // visual mesh
        MESHTYPE_COLLISION = 0x02,          // NOTE(zaklaus): Imaginary type based on mesh name "wcol*"
        MESHTYPE_SECTOR = 0x05,             // part of space, used for culling, effective lighting etc.
        MESHTYPE_DUMMY = 0x06,              // invisible bounding box
        MESHTYPE_TARGET = 0x07,             // used in human models (as a shooting target?)
        MESHTYPE_BONE = 0x0a;                // for skeletal animation

const  VISUALMESHTYPE_STANDARD = 0x0,      // normal mesh
    VISUALMESHTYPE_SINGLEMESH = 0x02,   // mesh with bones
    VISUALMESHTYPE_SINGLEMORPH = 0x03,  // combination of morph (for face) and skeletal (for body) animation
    VISUALMESHTYPE_BILLBOARD = 0x04,    // billboarding mesh (rotates towards camera
    VISUALMESHTYPE_MORPH = 0x05,        // mesh with morphing (non-skeletal) animation, e.g. curtains in wind
    VISUALMESHTYPE_GLOW = 0x06,         // has no geometry, only shows glow texture
    VISUALMESHTYPE_MIRROR = 0x08;        // reflects the scene

class Material {
    load = (reader)=> {
        this.flags = reader.readU32();
        this.ambient = reader.readVec();
        this.difuse = reader.readVec();
        this.emission = reader.readVec();
        this.transparency = reader.readFloat();

        if(this.flags & MATERIALFLAG_ENVIRONMENTMAP) {
            this.envRatio = reader.readFloat();
            const envMapNameLenght = reader.readU8();
            this.envMapName = reader.readString(envMapNameLenght);
        }

        const difuseMapNameLenght = reader.readU8();
        this.difuseMapName = reader.readString(difuseMapNameLenght);

        if(this.flags & MATERIALFLAG_ALPHATEXTURE) {
            const alphaMapNameLenght = reader.readU8();
            this.alphaMapName = reader.readString(alphaMapNameLenght);
        }

        if(this.flags & MATERIALFLAG_ANIMATEDTEXTUREDIFFUSE) {
            this.animSequenceLenght = reader.readU32();
            const unk0 = reader.readU16();
            this.framePeriod = reader.readU32();
            const unk1 = reader.readU32();
            const unk2 = reader.readU32();
        }

        return true;
    }
}

class Lod {
    load = (reader)=> {
        this.relativeDistance = reader.readFloat();
        const vertexCount = reader.readU16();

        this.vertices = [];
        for(let i = 0; i < vertexCount; i++) {
            const NewVertex = {
                pos: reader.readVec(), 
                normal: reader.readVec(),
                uv: {x: reader.readFloat(), y: reader.readFloat()}
            };
            this.vertices.push(NewVertex);
        }

        this.faceGroups = [];
        const faceGroupCount = reader.readU8();
        for(let i = 0; i < faceGroupCount; i++) {

            let newFaceGroup = {};
            newFaceGroup.faces = [];
            const faceCount = reader.readU16();

            for(let j = 0; j < faceCount; j++) {
                let newFace = {
                    A: reader.readU16(), 
                    B: reader.readU16(),
                    C: reader.readU16()
                };
                newFaceGroup.faces.push(newFace);
            }
     
            newFaceGroup.materialID = reader.readU16();
            this.faceGroups.push(newFaceGroup);
        }

        return true;
    }
}

class Mesh {
    constructor(meshType) {
        this.meshType = meshType;
    }   

    // dont abstract too much 
    // standard class not needed object is used instead
    loadStandard = (reader) => {
        let standard = {};
        standard.instanced = reader.readU16();
        standard.lods = [];

        if(!standard.instanced) {
            standard.lodLevel = reader.readU8();
            for(let i = 0; i < standard.lodLevel; i++) {
                let newLod = new Lod();
                if(newLod.load(reader)) {
                    standard.lods.push(newLod);
                }
            }
        }

        return standard;
    }

    loadPortal = (reader) => { 
        let portal = {}
        portal.vertexCount = reader.readU8();
        portal.vertices = []
        portal.unk0 = reader.readU32();
        portal.unk6 = [];

        for(let i = 0; i < 6; i++)
            portal.unk6.push(reader.readU32());

        for(let i = 0; i < portal.vertexCount; i++) {
            portal.vertices.push(reader.readVec());
        }

        return portal;
    }

    loadSector = (reader) => {
        let sector = {};
        sector.unk0 = reader.readU32()
        sector.unk1 = reader.readU32()
        sector.vertexCount = reader.readU32()
        sector.faceCount = reader.readU32()
        sector.vertices = []
        sector.faces = []
        sector.portals = []

        for(let i = 0; i < sector.vertexCount; i++) {
            sector.vertices.push(reader.readVec())
        }

        for(let i = 0; i < sector.faceCount; i++) {
            let newFace = {
                A: reader.readU16(), 
                B: reader.readU16(),
                C: reader.readU16()
            };
            sector.faces.push(newFace);
        }

        sector.minBox = reader.readVec();
        sector.maxBox = reader.readVec();
        sector.portalCount = reader.readU8();

        for(let i = 0; i < sector.portalCount; ++i) {
            const newPortal = this.loadPortal(reader);
            sector.portals.push(newPortal);
        }

        return sector;
    }

    load = (reader)=> {

        if(this.meshType === MESHTYPE_STANDARD) {
            this.visualMeshType = reader.readU8();
            this.renderFlags = reader.readU16();
        }

        this.parentID = reader.readU16(); // 0 - not connected
        this.pos = reader.readVec();
        this.scale = reader.readVec();
        this.rot = reader.readQuat();

        this.cullingFlags = reader.readU8();
        
        const meshNameLenght = reader.readU8();
        this.meshName = reader.readString(meshNameLenght);

        const meshParamsLenght = reader.readU8();
        this.meshParams = reader.readString(meshParamsLenght);

        switch(this.meshType) {
            case MESHTYPE_STANDARD: {
                switch(this.visualMeshType) {    
                    case VISUALMESHTYPE_STANDARD: {
                       this.standard = this.loadStandard(reader);
                    } 
                    break;
                }
            } break;

            case MESHTYPE_DUMMY: {
                this.dummy = {
                    minBox: reader.readVec(),
                    maxBox: reader.readVec()
                };
            } break;

            case MESHTYPE_SECTOR: {
                this.sector = this.loadSector(reader)
            } break;

            default: {
                console.log('[*] not implemented !');
            } break;
        }

        return true;
    }
}

class Model {
    constructor() {
        this.reader = null;
        this.version = 0;
        this.timestamp = 0;
        this.meshes = [];
    }

    loadMaterials = () => {
        let loadedMaterials = [];
        const materialCount = this.reader.readU16();
        
        for(let i = 0; i < materialCount; i++) {
            let newMaterial = new Material();
            if(newMaterial.load(this.reader)) {
                loadedMaterials.push(newMaterial);
            }
        }
        return loadedMaterials;
    }

    loadMeshes = () => {
        let loadedMeshes = [];
        const meshCount = this.reader.readU16();

        for(let i = 0; i < meshCount; i++) {
            const newMeshType = this.reader.readU8();
            let newMesh = new Mesh(newMeshType);
            if(newMesh.load(this.reader)) {
                loadedMeshes.push(newMesh);
            }
        }

        return loadedMeshes;
    }

    load = (data) => {
        this.reader = new BinaryReader(data);
        let sig = this.reader.readString(4);
        if(sig != "4DS\0") 
            return console.error("Ale ic do piči a daj mi validny modelik!");

        this.version = this.reader.readU16();
        if(this.version != 29) 
            return console.error("Model is not PC Version of mafia !");
        
        this.timestamp  = this.reader.readU64();
        this.materials  = this.loadMaterials();
        this.meshes     = this.loadMeshes();
        return true;
    } 
}
